import sys
import sysconfig
import distutils

def get_pydir():
    return 'lib.{platform}-cpython-{version[0]}{version[1]}'.\
            format(platform=sysconfig.get_platform(), version=sys.version_info)

if __name__ == '__main__':
    print(get_pydir())
